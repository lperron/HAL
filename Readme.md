# HAL
HAL (Heuristically programmed ALgorithmic computer) is a program using various meta-heuristic methods to approximate solutions of travelling salesman problems (NP-Hard problems).

## Dependencies

cpp make gsl root Mplayer

Mplayer is only useful if you want to make movies of your results.
http://www.mplayerhq.hu/design7/dload.html


For root... Good Luck!
https://root.cern.ch/

## Installation & Compilation

```
~$ git clone https://gitlab.com/lperron/HAL.git
~$ cd HAL/HAL/
~$ make -j2
```


## Usage

``` ~/HAL $ ./HAL/main ```
- 1.Choose a map
     - generate a random map or select a .tsp file in the "cartes" directory
- 2.Choose which algorhithm to run
     - BruteForce
     - Genetic
     - Ant Colony Optimization
     - Ant Colony System
     - Tabu Search
- 3.Run It!

## NB

For more informations you can read CRprojet.pdf (French) and there are movies of each algorhithms in the "Resultats" directory.
