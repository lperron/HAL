#ifndef _HAL_
#define _HAL_H_

#include <SysLin.h> 
#include <cfloat>

using namespace std;

class Cinnabar
{
protected :
int N;	//nombre de ville
Vecteur* distrib;	//distribution des villes
int* chemin;	//pour stocker un chemin
int *rapide;	//chemin le plus rapide
MatriceCarree Mdistances;	//matrice des distances
const char* Clogo;
public : 

Cinnabar(int Ni, string Slogos, Vecteur* distribi);
void permuter (int* chemin,int position1, int position2);	
double evalD(Vecteur point1,Vecteur point2);
void remplirDistances();
void affichelogo();
virtual double evalChemin()=0;
~Cinnabar() {delete[] chemin;delete[] rapide;}
};




class Mewtwo : public Cinnabar
{
private :
double distance;
public :
Mewtwo(int Ni,Vecteur* distribi):Cinnabar(Ni,"../logos/mewtwo.txt",distribi){distance=DBL_MAX;};
double evalChemin();	
void bruteforce(int pos);
void coutrapide();
};


class ant : public Cinnabar
{
protected :

double tailledesglandes;
double mauvaisevue;
double odorat;
double evap;
double pheromonesinit;
double sommeproba;
int maxiter;
Vecteur temp;
MatriceCarree Mvisibilite;
MatriceCarree Mpheromones;
int** Tchemins;
bool** dejavisitee;
double sommetemp;
double* Tdistances;
int nbfourmis;

public :

ant(int Ni,int popi,Vecteur* distribi);
virtual void prob(int i,int j)=0;
virtual void choixville(int i, int j)=0;
virtual void majpheromones()=0;
virtual void ecrireparams(string sdossier)=0;
void afficherresultat();
void remplirTabs();
void remplirMatrices();
void graphroot(int k,int contraste,string nomdossier);
~ant();

};


class ACO : public ant
{
private :

public :
ACO(int Ni,int popi,Vecteur* distribi) :ant(Ni,popi,distribi){};
void prob(int i,int j);
void choixville(int i, int j);
void evaporation();
void majpheromones();
void gofourmisgo();
double evalChemin(){return 0;};
void ecrireparams(string sdossier);

};


class ACS : public ant 
{
private :
double q0;
int **listevilles;
int tailleliste;
public:
void gofourmisgo();
void remplirliste();
void choixliste(bool& choixeffectue,int i,int j);
ACS(int Ni,int popi,Vecteur* distribi);
void choixville(int i,int j);
void prob(int i,int j);
void majpheromonesloc(int i, int j);
void majpheromones();
double evalChemin(){return 0;};
void ecrireparams(string sdossier);
~ACS();

};

class Genetique : public Cinnabar
{
	protected:
	int nbsurvivants; //Nombre de survivant via la SelectionRoulette
	int pop; //Nombre d'individu de la population
	int fmut; //frequence de mutation
	int nbiter; //nombre d'iterations contenant Selection, Croisement, et Mutation
	double alpha;
	int** Tchemins; //"Code genetique" de chaque individu -> pour chque individu ordre des villes visitées
	int* survivants; //Stock des numéros des survivants a la selectionroulette
	double* Dchemins; //Distance que parcours chaque individu
	double* fitness; //performance relative de chaque individu
	public:
	Genetique(int Ni,Vecteur* distribi);
	~Genetique();

	double evalChemin();
	void generestant(int enfant, int parent,int geneconserve);
	void Fitness();
	void SelectionRoulette();
	void TriSurvivants();
	void CroisementMPX();
	void Mutation();
	void CheminFinal();
	void Resolution();
	void groot(int k, string snomdossier);
};

class RechercheTabou : public Cinnabar
{
	protected :
	int* mSolution;
	int* mCandidat;
	int nCandidats;
	int** TabuList;
	int mem;
	int nbiter;
	double* fitness;
	double fitmSolution;
	int** Tchemins;
	public :
	RechercheTabou(int Ni, Vecteur* distribi);
	~RechercheTabou();
	void TabuSearch();
	bool Tabu(int k);
	void Candidat();
	double evalChemin();
	void afficherres();
	void groot(int k,string snomdossier);
};


Vecteur* Distribmap(int& N,string Smap,bool& halsuccess);
Vecteur* Distrib(int N, double xmax, double ymax);
void shutdownHAL(string& Smap);
void mkdirs(string nomdossier);
void avi(int nbiter, int f,string snomdossier);
#endif
