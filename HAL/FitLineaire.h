#ifndef _FITLINEAIRE_H_
#define _FITLINEAIRE_H_

#include <SysLin.h>

class FitLineaire {
 public:
  int N, M; // Nombre de points � fitter 
            // et nombre de fonctions de base
  Vecteur x, y, sig; // Les donn�es
  MatriceInversible alpha;   // La matrice alpha des �quations normales
  Vecteur beta;          // Le vecteur beta des �quations normales
  double gamma ;         // Sum_i (y_i/sig_i)^2 (pour le calcul de chi2) 
  Vecteur a_sol;         // Le vecteur solution

  // Methodes Get utiles :
  int get_M() { return M ; }
  Vecteur params_solution() { return a_sol ; }


 public:
  // Par defaut on defini un objet vide car c'est la lecture des donnees
  // qui fixera les attributs (autre que M que l'on passe en parametre).
  FitLineaire(int M_i){ N = 0; M=M_i; }

  // *************************************************************

  // Lecture de donn�es d'un fichier et stockage dans la classe
  void lire_donnees(const char * nom_fichier);

  // eval_f d�finit les fonctions de base
  
  virtual Vecteur eval_f(double x)=0;
  

  // Construction de lamatrice alpha et du vecteur beta des eq. norm.
  void construire_eqnormales();

  // R�solution : calcul de a_j et de la matrice de covariance (incertitudes)
  void resoudre_eqnormales();
  MatriceCarree matrice_covariance();

  // Ecriture des parametres
  void ecrire_params(const char * nom_fichier);  

  // Evaluation de la fonction d'ajustement trouv�e
  double eval_solution(double x);

  // Chi2
  double chi2() ;

  // Ecriture des donn�es ajustees
  void ecrire_solution(const char * nom_fichier);

  // *************************************************************

  /* M�thode ma�tresse qui r�alise l'ajustement sur un fichier 
     de donn�es (pas demandee dans le TP, mais bien pratique) */
  void ajuster_donnees(const char * nom_fichier);

  // *************************************************************

};

class FitHarmonique : public FitLineaire //la classe fitharmonique
{
private:
double omega;
public :
FitHarmonique(int M_i, double om) : FitLineaire(M_i) {omega=om;}	//constructeur initialise omega et appelle le constructeur de fitlineaire	
Vecteur eval_f(double x);//modelisation harmonique
};

class FitPolynom : public FitLineaire	//classe en bonus, pas tr�s utile dans notre cas, mais pas ininteressante
{
private :

public :
FitPolynom(int M_i) : FitLineaire(M_i){}
Vecteur eval_f(double x); //modelisation polynomiale
};

#endif
