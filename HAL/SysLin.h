#ifndef _SYSLIN_H_
#define _SYSLIN_H_

#include "Vecteur.h"
#include "MatriceCarree.h"

// Classe MatriceInversible 
// Elle herite de la classe MatriceCarre en mettant a disposition  
// la methode de decomposition LU et une methode
// de calcul de la matrice inverse de la classe MatriceCarree

class MatriceInversible : public MatriceCarree {
 public:
  // Constructeurs (cf. classe MatriceCarree)
  MatriceInversible(int dim_i = 1) : MatriceCarree(dim_i) {}
  MatriceInversible(int dim_i, const double diag) : 
    MatriceCarree(dim_i, diag) {}
  MatriceInversible(int dim_i, const double * Mtab_i) : 
    MatriceCarree(dim_i, Mtab_i) {}
  MatriceInversible(const MatriceCarree & m_i) : 
    MatriceCarree(m_i) {}
  // Decomposition LU de la matrice dans les matrices passees en argument
  void decompositionLU(MatriceCarree & L, MatriceCarree & U) const;
  // Calcul de la matrice inverse
  MatriceCarree inverse();
};

// Classe SystemeLineaire
// Elle permet de resoudre un système d'equations lineaires couplees soit par :
// -resolution de la decompositionLU
// - ou par méthodes iteratives 

class SystemeLineaire {
 private:
  MatriceCarree L, U;    // Stockees ici  	 
//  MatriceInversible & M; // Reference : en fait, stockee ailleurs // en commentaire pour l'heritage -> classe Ailette
  bool resoLU;           // Drapeau indiquant que la decomposition LU a ete operee
  int iter_max;          // Nombre max. d'iterations pour les methodes iteratives
  double tolerance;
  
  protected:
  MatriceInversible & 	M; // Reference : en fait, stockee ailleurs // Definition en "protected" de maniere a pouvoir definir cette matrice dans la classe Ailette
  int N;		// Dimension du systeme
  int Niter ;
  Vecteur		b; 
  Vecteur x;		// Solution
  
 public:
  // Constructeur : On initialise la classe en lui passant une MatriceInversible
  SystemeLineaire(MatriceInversible & M_i, int iter_max_i = 1000, double tolerance_i= 1.0E-12) : M(M_i), tolerance(tolerance_i)
    { resoLU = false; iter_max = iter_max_i; N = M_i.getdim(); x.redim(N); Niter = 0;}
  // Methode pour demander si la decomposition LU a ete effectuee
  bool est_resoLU() const { return resoLU; }
  // Decomposition LU de la matrice caracterisant le système
  void resoudreLU();
  void Set_b(const Vecteur & b);
  
  // Calcul de la solution pour un second membre donne, par decomposition LU
  void solutionLU() ;
  void solutionLU(const Vecteur & b) {Set_b(b);solutionLU();} 
  // Calcul de la solution pour un second membre donne, par resolution iterative
  int solutionJacobi() ;
  int solutionGaussSeidel() ;
  int solutionSOR(double w) ;
  
  Vecteur GetSolution() {return x;}
  void Set_ComposantesX(double k) {for (int i=1; i<=N; i++) x(i) = k;}
  void Reset_b() {for (int i=1; i<=N; i++) b(i) = 0;}
  int GetNiter() {return Niter;}
  MatriceInversible GetSystem() {return M;} 
  MatriceCarree GetL() {return L;}   
  MatriceCarree GetU() {return U;}   
};

#endif
