#include <cstdlib>
#include <iostream>
#include <cstring>
#include <fstream>
#include <cmath>
#include <cfloat>	//pour DBL_MAX
#include <ctime>
#include<SysLin.h>
#include "HAL.h"

using namespace std;

int main()
{
int N;
bool halsuccess;
int choixdistrib;
Vecteur* distrib=new Vecteur[42];
system("clear");
int menu=666;
int popi;
double tempmenu;
bool quit=false;
do{
	if(menu!=666)
	{
		do{
			system("clear");
			cout<<"============================= Menu ============================="<<endl<<endl;
			cout<<"1 : Bruteforce"<<endl<<"2 : Genetique"<<endl<<"3 : Ant colony"<<endl<<"4 : Ant colony system"<<endl<<"5 : Tabu search"<<endl<<"6 : Changer de carte"<<endl<<"7 : Quit"<<endl;
			cout<<endl<<"Choix : ";
			cin>>tempmenu;
			menu=tempmenu;
		}while(menu<1 or (menu>7 and menu!=42));
	}
	else
		menu=6;


	switch (menu)
	{

	case 1 :
	{
		Mewtwo M(N,distrib);
		system("clear");
		M.bruteforce(1);
		cout<<endl<<endl;
		M.coutrapide();
		cout<<endl<<endl;
		break;
	}
	
	case 2 :
	{
		system("clear");
		Genetique G(N,distrib);
		G.Resolution();
		break;
	}

	case  3 :
	{	
		do{
			system("clear");
			cout<<"nombre de fourmis =?"<<endl;
			cin>>popi;
			cout<<endl;}while(popi<1);
		ACO A(N,popi,distrib);
		A.gofourmisgo();
		cout<<endl;
		break;
	}

	case 4 :
	{
		do{
			system("clear");
			cout<<"nombre de fourmis =?"<<endl;
			cin>>popi;
		cout<<endl;}while(popi<1);
		ACS As(N,popi,distrib);
		As.gofourmisgo();
		cout<<endl;
		break;
	}
	
	case 5 :
	{
		system("clear");
		RechercheTabou T(N,distrib);
		T.TabuSearch();
		break;
	}

	case 6 :
	{
		delete[] distrib;
		system("clear");
	do{
		cout<<"============================= Type de Distribution ============================="<<endl<<endl;
		cout<<"1 : Distribution aleatoire"<<endl<<"2 : Lecture de fichier"<<endl;
		cout<<endl<<"Choix: ";
		cin>>choixdistrib;
		system("clear");}while(choixdistrib!=1 and choixdistrib!=2);
	if(choixdistrib==1)
	{
		int X,Y;
		cout<<"============================= Distribution Aleatoire ============================="<<endl<<endl;
		cout<<"Nombre de villes=?"<<endl;
		do{
			cin>>N;}while(N<2);
		cout<<"X max=?"<<endl;
		cin>>X;
		cout<<"Y max =?"<<endl;
		cin>>Y;
		distrib=Distrib(N,X,Y);
		cout<<endl;
		cout<<"Points :"<<endl<<endl;
		for(int i=0;i<N;i++)
			cout<<distrib[i]<<endl;
		cout<<endl;
	}
	if(choixdistrib==2)
	{
		do{
			cout<<"============================= Lecture de Fichier ============================="<<endl<<endl;
			cout<<"Fichiers disponibles :";
			string smap;
			cout<<endl;
			system(" ls ../cartes");
			cout<<endl<<"Nom du fichier ?"<<endl;
			cin>>smap;
			distrib=Distribmap(N,smap,halsuccess);}while(halsuccess==false);
	}	


		break;
	}

	case 7 :
	{quit=true;break;}
	
	
	case 42 :
	{
		system("clear");
		ifstream fman("../man.txt");
  		string lignes = "";        
		while (fman.good ())	//on remplit lignes
		{
		    string temp;                 
		    getline (fman , temp);        
		    temp += "\n";                      
	 	   lignes += temp;                     
		}
		cout<<lignes;	//et on affiche
		break;
	}
	}
	if(quit!=true)
		system("read akwakwak");

}while(quit==false);
delete[] distrib;
system("clear");
return 0;
}
