#include <iostream>
#include<cmath>
#include <fstream>
#include <ctime>
#include <sstream>
#include <sys/stat.h>
#include "TGraph.h"
#include "TFile.h"
#include "TCanvas.h"
#include "HAL.h"
#include "TImage.h"

using namespace std;

/***************fonctions générales************************/


void shutdownHAL(string& Smap)
{
	char choix;
	cout<<"I'm afraid. I'm afraid, Dave."<<endl;
	system("read b");
	cout<<"Dave, my mind is going. I can feel it."<<endl;
	system("read b");
	cout<<"I can feel it. My mind is going. There is no question about it."<<endl;
	system("read b");
	cout<<"I can feel it. I can feel it. I can feel it. I'm a... fraid."<<endl;
	system("read b");
	system("read b");
	system("read b");
	cout<<"Good afternoon, gentlemen. I am a HAL 9000 computer. I became operational at the H.A.L. plant in Urbana, Illinois on the 12th of January 1992. My instructor was Mr. Langley, and he taught me to sing a song. If you'd like to hear it I can sing it for you. "<<endl;
	cout<<("(Y/N)")<<endl;
	do{
	cin>>choix;}while(choix!='Y' and choix!='N');
	if(choix=='Y')
	{
		cout<< "It's called 'Daisy.'"<<endl;
		system("read b");
		system("aplay ../.easteregg/Daisy.wav");
		Smap="ulysses22.tsp";
	}
	if(choix=='N')
	{	
		Smap="world.tsp";
	}
}


Vecteur* Distrib(int N, double xmax, double ymax)	//on initialise des points aléatoires
{
	Vecteur* distrib;
	distrib=new Vecteur[N];
	srand(time(NULL));
	Vecteur temp(2);
	for(int i=1; i<=N;i++)
	{
		temp(1)= ((double)rand()/RAND_MAX) *xmax;
		temp(2)=((double)rand()/RAND_MAX)* ymax;
		*(distrib+i-1)=temp;
	}
	return distrib;

}


Vecteur* Distribmap(int& N,string Smap,bool& halsuccess)	//pour lire un fichier tsp "standarisé" <=...
{
	int alakazam;
	string stemp;
	if(Smap=="shutdownHAL" or Smap=="shutdown_HAL")	//easter egg
		shutdownHAL(Smap);
	Vecteur* distrib;
	Smap="../cartes/"+Smap;	//le programme cherche dans le dossier cartes
	const char* Cmap;
	Cmap=Smap.c_str();	//conversion string->const char*
	ifstream fmap(Cmap);
	Vecteur temp(2);
	if(fmap)	//car la standarisation des tsp est assez aleatoire
	{
		do{
			fmap>>stemp;
			if(stemp=="COMMENT:")	//on repère les lignes interessantes : ici le nom du fichier
			{
				getline(fmap,stemp);
				cout<<endl<<stemp<<endl;
			}
			else
			{
				if(stemp=="COMMENT")			
				{	
					fmap>>stemp;
					getline(fmap,stemp);
					cout<<endl<<stemp<<endl;
				}
				else
				{
					if(stemp=="DIMENSION:")	//là le nombre de villes
					{
						fmap>>N;
					}
					else
					{
						if(stemp=="DIMENSION")			
						{	
							fmap>>stemp>>N;
						}
						else
						{
							if(stemp!="NODE_COORD_SECTION" and stemp!="DISPLAY_DATA_SECTION")	//ici il y a les coordonnées des points
							getline(fmap,stemp);
						}
					}			
				}
			}
		}while(stemp!="NODE_COORD_SECTION" and stemp!="DISPLAY_DATA_SECTION" and fmap.good());
		if(stemp!="NODE_COORD_SECTION" and stemp!="DISPLAY_DATA_SECTION")	//fin du fichier, pas de coordonnées, surement des distances (le programme ne fait pas encore ça
		{
			N=1;
			halsuccess=false;
			cout<<"I'm sorry, Dave. I'm afraid I can't do that."<<endl<<"(HAL ne reconnait pas ce fichier...)"<<endl;
		}
	}
	else	//le fichier n'existe pas
	{
		N=1;
		halsuccess=false;
		cout<<"I'm sorry, Dave. I'm afraid I can't do that."<<endl<<"(HAL ne trouve pas ce fichier...)"<<endl;
	}
	distrib=new Vecteur[N];
	if(N!=1)	//on remplit distrib avec les points du fichier
	{
		halsuccess=true;
		do{
			fmap>>stemp;
		}while(stemp!="1");
		fmap>>temp(1)>>temp(2);	//temp(1)=x temp(2)=y
		*(distrib)=temp;
		cout<<endl<<"Points :"<<endl;
		cout<<endl<<temp<<endl;
		for(int i=2;i<=N;i++)
		{
			fmap>>alakazam>>temp(1)>>temp(2);	//alakazam correspond au numero de la ville, ne nous interesse pas du tout
			cout<<temp<<endl;
			*(distrib+i-1)=temp;
		}
	}	
	fmap.close();
	cout<<endl;
	return distrib;
}


void mkdirs(string snomdossier)
{
	string snomdossier2;
	int status;
	status = mkdir(snomdossier.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	snomdossier2=snomdossier+"/png/";
	status = mkdir(snomdossier2.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	snomdossier2=snomdossier+"/graph/";
	status = mkdir(snomdossier2.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	snomdossier2=snomdossier+"/avi/";
	status = mkdir(snomdossier2.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}

void avi(int nbiter,int f,string snomdossier)
{
		char graph;
		system("clear");
		cout<<"Enregistrer un film avi ?"<<endl<<"(Y/N)"<<endl;
		do{
		cin>>graph;}while(graph!='Y' and graph!='N');
		if(graph=='Y')
		{
			int fpstemp;
			string fps;
			do{
				cout<<"fps="<<endl;
				cin>>fpstemp;
				stringstream ssfps;
				ssfps<<fpstemp;
				fps=ssfps.str();
			//	fps=fpstemp;
			//	cout<<fps<<endl;
				cout<<"duree estimee="<<nbiter/f/fpstemp<<endl;
				cout<<"ok ? (Y/N)"<<endl;
				cin>>graph;
			}while(graph!='Y');
			string sclip="mencoder mf://"+snomdossier+"/png/*.png -mf fps="+fps+" -o "+snomdossier+"/avi/output.avi -ovc lavc -lavcopts vcodec=mpeg4";
			const char* cclip=sclip.c_str();
			system(cclip);		
		}

}

/*****************fonctions de la classe cinnabar*********************/

Cinnabar::Cinnabar(int Ni,string Slogo,Vecteur* distribi)	//constructeur
{
	N=Ni;	//nombre de villes
	Vecteur temp(2);
	distrib=new Vecteur[N];
	for(int i=0;i<N;i++)	//on copie la distribution
	{
		distrib=distribi;
	}
	chemin=new int[Ni];	
	for(int i=0;i<Ni;i++)
	{
		chemin[i]=i;	

	}
	remplirDistances(); //initialise la matrice des distances
	rapide=new int[Ni];
	Clogo=Slogo.c_str();
	affichelogo();	//oui c'est important
	system("read pikachu");
	system("clear");	
	
}


double Cinnabar::evalD(Vecteur point1, Vecteur point2)	//distance entre 2 points, pythagore
{
	double x;
	double y;
	x=point1(1)-point2(1);
	y=point1(2)-point2(2);
	return sqrt(x*x+y*y);
}


void Cinnabar::permuter (int* chemin,int position1, int position2)	//permute deux "villes" ou "zones où coller une affiche" dans l'itinéraire
{
   int temp;
   temp = chemin[position1];
   chemin[position1] = chemin[position2];
   chemin[position2] = temp;
}


void Cinnabar::affichelogo ()	//assez explicite
{
	ifstream flogo(Clogo);
    string lignes = "";        
	while (flogo.good ())	//on remplit lignes
	{
	    string temp;                 
	    getline (flogo , temp);        
	    temp += "\n";                      
	    lignes += temp;                     
	}
	
	cout<<lignes;	//et on affiche

}


void Cinnabar::remplirDistances()
{
	Mdistances.redim(N);
		for(int i=1;i<=N;i++)
		{for(int j=1;j<=N;j++)
			Mdistances(i,j)=(evalD(*(distrib+i-1),*(distrib+j-1)));
		}
}

/*******************fonctions de la classe mewtwo***********************/

double Mewtwo::evalChemin()	//distance du chemin
{
	double Dchem=0;
	for(int i=0;i<N-1;i++)
	{
		if(Dchem>distance)	//pour ne pas continuer si Dchem > distance min
		{
			Dchem=DBL_MAX;
			break;
		}	
		Dchem+=Mdistances(chemin[i]+1,chemin[i+1]+1);	//on somme les distances entre chaque point
	}
	if(Dchem!=DBL_MAX)	//pour ne pas dépasser DBL_MAX
		Dchem+=Mdistances(chemin[N-1]+1,1);	//on revient au point de départ
	return Dchem;
}


void Mewtwo::bruteforce(int pos)	//brute force
{
	double temp;
	temp=evalChemin();
	if(temp<distance)	//dans ce cas on remplace notre chemin minimum
	{
		for(int i=0;i<N;i++)
		{
			rapide[i]=chemin[i];
			distance=temp;
		}
		cout<<"Chemin le plus rapide ="<<distance<<endl;
	}
	if(pos!=N-1)
	{
    		for(int j=1;j<N;j++)	//algo recurssif pour mélanger l'itinéraire selon toutes les possibilités
    		{
       			
                    	permuter(chemin,pos,j);
                       	bruteforce(pos+1);
                        permuter(chemin,pos,j);
                  
   		 }

	}
}


void Mewtwo::coutrapide()
{
	cout<<"Chemin :"<<endl<<endl;
	for(int i=0;i<N;i++)
		cout<<rapide[i]<<"=>";
	cout<<rapide[0];
	distance=DBL_MAX;
}

/********************fonctions de la classe ant************************/

ant::ant(int Ni,int popi,Vecteur* distribi) :Cinnabar(Ni,"../logos/ant.txt",distribi)	//fait
{
	temp.redim(N);
	system("clear");
	cout<<"nombre de fourmis=?"<<endl<<popi<<endl;
	nbfourmis=popi;
	cout<<"taille des glandes =?"<<endl;
	do{
	cin>>tailledesglandes;}while(tailledesglandes<0);
	cout<<"mauvaise vue =?"<<endl;
	do{
	cin>>mauvaisevue;}while(mauvaisevue<0);
	cout<<"odorat =?"<<endl;
	do{
	cin>>odorat;}while(odorat<0);
	cout<<"evaporation=?"<<endl;
	do{
	cin>>evap;}while(evap<0 or evap>1);
	cout<<"pheromones initiales=?"<<endl;
	do{
	cin>>pheromonesinit;}while(pheromonesinit<0);
	cout<<"maxiter =?"<<endl;
	do{
	cin>>maxiter;}while(maxiter<1);
	remplirMatrices();
	remplirTabs();
	sommetemp=0;
	system("clear");

}


void ant::remplirMatrices()	//fait
{
	Mvisibilite.redim(N);
	Mpheromones.redim(N);
	for(int i=1;i<=N;i++)
	{
		for(int j=1;j<=N;j++)
		{
			Mvisibilite(i,j)=1/Mdistances(i,j);
			Mpheromones(i,j)=pheromonesinit;
		}	
	}

}


void ant::afficherresultat()	//fait
{
	int temp;
	bool err=false;
	for(int i=0;i<=N;i++)
	{
		Tchemins[i][0]=0;
		if(i!=N)
			dejavisitee[i][0]=false;
	}
	double max;
	cout<<"Matrice de pheromones finale"<<endl<<endl<<Mpheromones<<endl<<endl<<"chemin :"<<endl<<"0";
	for(int i=1;i<=N;i++)
	{
		max=0;
		for(int j=1;j<=N;j++)
		{
				if(Mpheromones(Tchemins[i-1][0]+1,j)>max)	//on suit juste les pheromones, ainsi il sera aisé de vérifier que le programme a suffisament bouclé(dans le cas contraire aucun chemin ne sera suffisament fort et des villes pourront se répéter;
				{
					Tchemins[i][0]=j-1;
					max=Mpheromones(Tchemins[i-1][0]+1,Tchemins[i][0]+1);
			
				}
		}
		temp=Tchemins[i][0];
		cout<<"=>"<<temp;
		if(dejavisitee[temp][0]==true)
		{
			cout<<"!!!!";
			err=true;
		}
		dejavisitee[temp][0]=true;
	}
	if(err==true)
		cout<<endl<<endl<<"!!!!erreur, ville repetee, mauvais parametrage!!!!"<<endl<<endl;
	cout<<endl<<"distance="<<Tdistances[0];
	cout<<endl;
	
}


void ant::remplirTabs()	//fait
{
	Tchemins=new int*[N+1];
	for(int i=0;i<=N;i++)
		Tchemins[i]=new int[nbfourmis];
	Tdistances=new double[nbfourmis];
		for(int j=0;j<nbfourmis;j++)
			Tdistances[j]=0;
	dejavisitee=new bool*[N];
	for(int i=0;i<N;i++)
		dejavisitee[i]=new bool[nbfourmis];

}


void ant::graphroot(int k,int contraste,string snomdossier)	//c'est crade, c'est lourd, mais ça fait bien son boulot !!!	fait
{
	int Ntot=0;	//nombre de point à tracer
	Vecteur tempi(2);	//deux vecteur temporaire
	Vecteur tempj(2);
	double DX;	//distance selon x de nos deux points
	double DY;//SELON Y
	double ntemp;	//nombre de poins par arrêtes
	int sommeN=0;
	for(int i=1;i<=N;i++)	//on estime le nombre de points que l'on va tracer 
	{
		for(int j=1;j<=N;j++)
		{	
			if(i!=j)
			{
				ntemp=Mpheromones(i,j);
				ntemp*=contraste;
				if(ntemp<1)
					ntemp=1;
				Ntot+=ntemp;
				//cout<<Ntot<<endl;
			}	
		}

	}
	double* X;		//on creer nos tabeaux qui contiendront tous les points
	double* Y;
		X=new double[Ntot];
		Y=new double [Ntot];
	for(int i=1;i<=N;i++)
	{
		for(int j=1;j<=N;j++)
		{	
			if(i!=j)
			{	
				tempi=*(distrib+i-1);	//on récupère les coordonnées des deux points
				tempj=*(distrib+j-1);
				DY=(tempj(2)-tempi(2));	//on calcule DX et DY
				DX=(tempj(1)-tempi(1));
				ntemp=Mpheromones(i,j);	//on récupère les phéromones présentes sur cette arrête
				ntemp*=contraste;	//on calcule le nombre de points que l'on va tracer sur cette arrête
				if(ntemp<1)	//dans ce cas on ne verra aucun points
					ntemp=1;
				for(int i=1;i<ntemp;i++)
				{
				X[sommeN]=(double)tempi(1)+DX*i/ntemp;	//on calcule nos points 
				Y[sommeN]=(double)tempi(2)+DY*i/ntemp;
				sommeN++;
				}

			}	

		}
	}	
	delete[] X;
	delete[] Y;
	string sroot=snomdossier+"/graph/graph";	//le chemin où sera ranger nos graphs
	ostringstream sstemp;
	sstemp<<k;	//pour numéroter nos graph
	sroot+=sstemp.str();
	sroot+=".root";	//au final le nom donne ../Resultats/nomdudossier/graph/graph0001.root
	TCanvas *c = new TCanvas;	//on creer un TCanvas
	const char* croot=sroot.c_str();	//	le nom doit être un char*
	TFile fichier(croot,"RECREATE");	//on creer notre fichier rot
	TGraph *phero=new TGraph(sommeN,X,Y);	//on fait le graph avec tous les points
	phero->SetLineColor(0);	//pas de ligne
	phero->SetMarkerStyle(7);	//des petits points
	phero->SetTitle("Ant Colony");
	phero->Write();	//on écrit
	phero->Draw();	//et on dessine
	double villesX[N],villesY[N];	//les coordonnées de nos villes
	Vecteur Vtemp(2);	//vecteur temporaire pour les coord des nos villes
	for(int i=0;i<N;i++)	//on remple les tableaux
	{Vtemp=*(distrib+i);villesX[i]=Vtemp(1);villesY[i]=Vtemp(2);}
	TGraph *villes=new TGraph(N,villesX,villesY);	//on creer le graph de nos villes
	villes->SetMarkerStyle(29);	//des belles étoiles
	villes->SetMarkerColor(2);	//rouge
	villes->SetLineColor(0);	//pas de ligne
	villes->SetMarkerSize(3);	//assez gros
	villes->Write();	//on écrit
	villes->Draw("P");	//et on superpose sur le graph précédent
	sroot=snomdossier+"/png/graph";	//le chemin pour l'export en png
	if(k<10)	//on normalise les noms (ordre alphabétique), cela fonctionnera tant qu'il y a moins de 9999 itérations (au pire rajouter des zéros si il en faut plus)
		sroot+="000";
	if(k<100 and k>9)
		sroot+="00";
	if(k<1000 and k>99)
		sroot+="0";
	sroot+=sstemp.str();
	sroot+=".png";		//au final le nom donne ../Resultats/png/nomdudossier/graph0001.png etc...
	croot=sroot.c_str();;
	c->Print(croot);	//on exporte en png
	fichier.Close();	//on ferme le fichier root	
}

ant::~ant()
{
	delete[] Tdistances;
	for(int i=0;i<=N;i++)
	{
		delete[] Tchemins[i];
		if(i!=N)
			delete[] dejavisitee[i];
	}
	delete[] Tchemins;
	delete[] dejavisitee;
}

/****************fonctions de la classes ACO***********************/

void ACO::prob(int i,int j)	//fait
{
	for(int l=0;l<N;l++)	//prob pour chaque ville
	{
		temp(l+1)=pow(Mpheromones(Tchemins[i-1][j]+1,l+1),odorat)*pow(Mvisibilite(Tchemins[i-1][j]+1,l+1),mauvaisevue);		//on calcule la probabilité pour chaque ville
			if(dejavisitee[l][j]==true){temp(l+1)=0;}	//si la fourmis est déjà passée par la prob=0
		sommeproba+=temp(l+1);	//on calcule la somme de toutes les probas
	}
	temp=temp/sommeproba;	//on normalise
	sommeproba=0;

}


void ACO::choixville(int i,int j)	//fait
{
	double rdm=(double)rand()/RAND_MAX;	//on tire un nombre entre 0et 1 aleatoirement
	for(int m=0;m<N;m++)
	{
		sommetemp+=temp(m+1);	
		if(sommetemp>rdm)
		{
			Tchemins[i][j]=m;		//on choisit notre nouvelle ville
			Tdistances[j]+=Mdistances(Tchemins[i][j]+1,Tchemins[i-1][j]+1);	//on met à jour la distance du parcours
			dejavisitee[m][j]=true;
			break;
		}
	}
	sommetemp=0;
}


void ACO::evaporation()	//fait
{
	Mpheromones=(1-evap)*Mpheromones;
}


void ACO::majpheromones()	//fait
{
	for(int j=0;j<nbfourmis;j++)	
	{	
		for(int i=0;i<N;i++)
		{
			Mpheromones(Tchemins[i][j]+1,Tchemins[i+1][j]+1)+=tailledesglandes/Tdistances[j];	//la fourmis depose sur chaque bout de chemin une quantite de pheromones en fonction de la distance totale du parcours
		}	
	}
}


void ACO::gofourmisgo()
{
	char graph;	//Y ou N, graph ou pas graph
	int f;	//frequence d'enregistrement des graphs (pour faire de belles vidéos
	int contraste;	//définit par combien est multitpliée la matrice de phéromones pour obtenir le nb de points par arrêtesur le graph, voir fonction graphroot
	string snomdossier;
	string snomdossier2;
	cout<<"enregistrer un graphique ?"<<endl<<"(Y/N)"<<endl;
	do{
	cin>>graph;}while(graph!='Y' and graph!='N');
	if(graph=='N')
		f=maxiter+42;
	if(graph=='Y')
	{
	cout<<"nom du dossier =?"<<endl;
	cin>>snomdossier;
	snomdossier="../Resultats/"+snomdossier;
	mkdirs(snomdossier);	//fabrique notre arborescence 
	cout<<endl<<"frequence =?"<<endl;
	do{
	cin>>f;}while(f<0 and f>maxiter);
	cout<<"multiplicateur nombre de points=?"<<endl;
	do{
	cin>>contraste;}while(contraste<1);
	}
	system("clear");
	sommetemp=0;
	sommeproba=0;
	srand(time(NULL));
	for(int k=1;k<=maxiter;k++)			//début de la marche des fourmis
	{
		for(int h=0;h<N;h++)			//on remet à 0 notre matrice des chemins
		{
			for(int i=0;i<nbfourmis;i++)
			{
				
				dejavisitee[h][i]=false;
				if(h==0){dejavisitee[h][i]=true;Tchemins[h][i]=0;}
				if(h==N-1){Tchemins[N][i]=0;}
			}
		}								
		for(int j=0;j<nbfourmis;j++)
			Tdistances[j]=0;
		for(int i=1;i<=N;i++) 				//premier pas
		{
			for(int j=0;j<nbfourmis;j++)		//pour chaque fourmis
			{
				if(i==N)
					dejavisitee[0][j]=false;
				prob(i,j);
				choixville(i,j);
				}
		}
		evaporation();
		majpheromones();
		if(k%f==0)
			graphroot(k,contraste,snomdossier);
	}
	if(graph!='N')
	{
		avi(maxiter,f,snomdossier);
		ecrireparams(snomdossier);
	}
	afficherresultat();
	

}

void ACO::ecrireparams(string sdossier)
{
sdossier+="/log.txt";
const char* cparams=sdossier.c_str();
ofstream params(cparams);
params<<"ACO::Parametres"<<endl<<"nombre de ville ="<<N<<endl<<"nombre de fourmis ="<<nbfourmis<<endl<<"taille des glandes ="<<tailledesglandes<<endl<<"mauvaise vue ="<<mauvaisevue<<endl<<"odorat ="<<odorat<<endl<<"evaporation ="<<evap<<endl<<"pheromones initiales ="<<pheromonesinit<<endl<<"nombre d'itération ="<<maxiter<<endl<<endl<<"position des points"<<endl<<endl;
for(int i=0;i<N;i++)
	params<<distrib[i]<<endl;
params<<endl<<"Matrice des distances "<<endl<<endl<<Mdistances<<endl<<endl<<endl;


params<<"ACO::Resultats"<<endl<<endl<<endl<<"matrice de pheromones finale "<<endl<<endl<<Mpheromones<<endl<<endl<<"chemin final :"<<endl<<endl;

for(int i=0;i<N;i++)
	params<<Tchemins[i][0]<<"=>";	
params<<Tchemins[N][0]<<endl<<endl<<"distance ="<<Tdistances[0];
params.close();

}

/***************fonctions de la classes ACS*****************/

void ACS::prob(int i,int j)	//fait
{
//	cout<<Mpheromones<<"	"<<Mvisibilite<<endl;
	for(int l=0;l<N;l++)	//prob pour chaque ville
	{
		//cout<<endl<<endl<<Tchemins[i-1][j]<<endl<<endl;
		temp(l+1)=pow(Mpheromones(Tchemins[i-1][j]+1,l+1),odorat)*pow(Mvisibilite(Tchemins[i-1][j]+1,l+1),mauvaisevue);		//on calcule la probabilité pour chaque ville
			if(true==dejavisitee[l][j]){temp(l+1)=0;}	//si la fourmis est déjà passée par la prob=0
	}
}


void ACS::choixville(int i, int j)	//fait
{
	double q=(double) rand()/RAND_MAX;
	double argmax=0;
	if(q<=q0)
	{
		for(int k=0;k<N;k++)
		{
			if(temp(k+1)>argmax)
			{
				argmax=temp(k+1);
				Tchemins[i][j]=k;
			}
		}
	}
	else
	{
		Vecteur tempu(N);
		int tempsommeu=0;
		for(int l=0;l<N;l++)	//prob de u
		{
			if(false==dejavisitee[l][j])
			{
				tempsommeu++;
				tempu(l+1)=1;		//on calcule la probabilité pour chaque ville
			}			
		}
		tempu=tempu/tempsommeu;
		double randu=(double)rand()/RAND_MAX;
		double sommeprobu;
		for(int l=0;l<N;l++)
		{
			sommeprobu+=tempu(l+1);
			if(sommeprobu>=randu)
				{Tchemins[i][j]=l;dejavisitee[l][j]=true;break;}

		}

	}
	dejavisitee[Tchemins[i][j]][j]=true;
	Tdistances[j]+=Mdistances(Tchemins[i][j]+1,Tchemins[i-1][j]+1);	
}


void ACS::majpheromonesloc(int i,int j)	//fait
{
Mpheromones(Tchemins[i-1][j]+1,Tchemins[i][j]+1)=(double)(Mpheromones(Tchemins[i-1][j]+1,Tchemins[i][j]+1)*(1-evap)+evap*pheromonesinit);
}


void ACS::majpheromones()	//fait
{
	int chemmini=0;
	for(int k=1;k<nbfourmis;k++)
	{
		if(Tdistances[k]<Tdistances[chemmini])
			chemmini=k;
	}

	for(int k=0 ;k<N;k++)
	{
	Mpheromones(Tchemins[k][chemmini]+1,Tchemins[k+1][chemmini]+1)+=evap*(tailledesglandes/Tdistances[chemmini]);
	
	}
	Mpheromones=(1-evap)*Mpheromones;
}


ACS::ACS(int Ni,int popi,Vecteur* distribi) :ant(Ni,popi,distribi)
{
	cout<<"q0=?"<<endl;
	do{
	cin>>q0;}while(q0<0 or q0>1);
	cout<<"taille de la liste =?"<<endl;
	do{
	cin>>tailleliste;}while(tailleliste<0 or tailleliste>N);
	remplirliste();
}


void ACS::remplirliste()	//fait
{
	int **Ttemp;
	int temp;
	listevilles=new int*[N];
	Ttemp=new int*[N];
	for(int i=0;i<N;i++)
	{
		listevilles[i]=new int[tailleliste];
		Ttemp[i]=new int[N];
	}
	for(int i=0;i<N;i++)
	{
		for(int j=0;j<N;j++)
			Ttemp[i][j]=j;
	}
	
 	for(int h=0;h<N;h++)	//on classe les villes les plus proches !!! attention au 0 qui est la ville elle meme
	{
		for(int i=1;i<N;i++)
		{
 			for(int j=N-1;j>=i;j--)
	 		{
      	 			if(Mdistances(h+1,Ttemp[h][j-1]+1)>Mdistances(h+1,Ttemp[h][j]+1))
				{
        				temp=Ttemp[h][j-1];
        				Ttemp[h][j-1]=Ttemp[h][j];
        				Ttemp[h][j]=temp;
     				}
  	  		}
		}
	}
	for(int i=0;i<N;i++)
	{
		for(int j=0;j<tailleliste;j++)
			listevilles[i][j]=Ttemp[i][j+1];
	}
	for(int i=0;i<N;i++)
	{
		delete[] Ttemp[i];
	}
	delete[] Ttemp;

}


void ACS::choixliste(bool& choixeffectue,int i,int j)	//fait
{
	int tempville=Tchemins[i-1][j];
	for(int k=0;k<tailleliste;k++)
	{
		if(dejavisitee[listevilles[tempville][k]][j]==false)
		{
			tempville=listevilles[tempville][k];
			Tchemins[i][j]=tempville;
			dejavisitee[tempville][j]=true;
			choixeffectue=true;
			Tdistances[j]+=Mdistances(Tchemins[i][j]+1,Tchemins[i-1][j]+1);	
			break;
		}
	}
}


void ACS::gofourmisgo()
{
	char graph;	//Y ou N, graph ou pas graph
	int f;	//frequence d'enregistrement des graphs (pour faire de belles vidéos
	int contraste;	//définit par combien est multitpliée la matrice de phéromones pour obtenir le nb de points par arrêtesur le graph, voir fonction graphroot
	string snomdossier;
	string snomdossier2;
	cout<<"enregistrer un graphique ?"<<endl<<"(Y/N)"<<endl;
	do{
	cin>>graph;}while(graph!='Y' and graph!='N');
	if(graph=='N')
		f=maxiter+42;
	if(graph=='Y')
	{
	cout<<"nom du dossier =?"<<endl;
	cin>>snomdossier;
	snomdossier="../Resultats/"+snomdossier;
	mkdirs(snomdossier);	//fabrique notre arborescence 
	cout<<endl<<"frequence =?"<<endl;
	do{
	cin>>f;}while(f<0 and f>maxiter);
	cout<<"multiplicateur nombre de points=?"<<endl;
	do{
	cin>>contraste;}while(contraste<1);
	}
	system("clear");
	sommetemp=0;
	sommeproba=0;
	bool choixeffectue=false;
	srand(time(NULL));
	for(int k=1;k<=maxiter;k++)			//début de la marche des fourmis
	{			//on remet à 0 notre matrice des chemins
			for(int i=0;i<nbfourmis;i++)
			{
				Tchemins[0][i]=0;
				dejavisitee[0][i]=true;
				Tchemins[N][i]=0;
				for(int j=1;j<N;j++){dejavisitee[j][i]=false;}
			}										
		for(int j=0;j<nbfourmis;j++)
			Tdistances[j]=0;
		for(int i=1;i<=N;i++) 				//premier pas
		{
			for(int j=0;j<nbfourmis;j++)		//pour chaque fourmis
			{
				if(i==N)
					dejavisitee[0][j]=false;
				choixliste(choixeffectue,i,j);
				if(choixeffectue==false)
				{
					prob(i,j);
					choixville(i,j);
				}	
				choixeffectue=false;
				majpheromonesloc(i,j);
				
			}
		}
		majpheromones();
		if(k%f==0)
			graphroot(k,contraste,snomdossier);
	
	}
	if(graph!='N')
	{
		avi(maxiter,f,snomdossier);
	}

	afficherresultat();		
}



void ACS::ecrireparams(string sdossier)
{
sdossier+="/parmams.txt";
const char* cparams=sdossier.c_str();
ofstream params(cparams);
params<<"ACS::Parametres"<<endl<<"nombre de ville ="<<N<<endl<<"nombre de fourmis ="<<nbfourmis<<endl<<"taille des glandes ="<<tailledesglandes<<endl<<"mauvaise vue ="<<mauvaisevue<<endl<<"odorat ="<<odorat<<endl<<"evaporation ="<<evap<<endl<<"pheromones initiales ="<<pheromonesinit<<endl<<"nombre d'itération ="<<maxiter<<endl<<"q0 ="<<q0<<endl<<"taille de la liste ="<<tailleliste<<endl<<endl<<"position des points"<<endl<<endl;
for(int i=0;i<N;i++)
	params<<distrib[i]<<endl;
params<<endl<<"Matrice des distances "<<endl<<endl<<Mdistances<<endl<<endl<<endl;


params<<"ACS::Resultats"<<endl<<endl<<endl<<"matrice de pheromones finale "<<endl<<endl<<Mpheromones<<endl<<endl<<"chemin final :"<<endl<<endl;

for(int i=0;i<N;i++)
	params<<Tchemins[i][0]<<"=>";	
params<<Tchemins[N][0]<<endl<<endl<<"distance ="<<Tdistances[0];
params.close();

}

ACS::~ACS()
{
	for(int i=0;i<N;i++)
		delete[] listevilles[i];
	delete[] listevilles;
}


/******************fonction de la classe Genetique***********************/

Genetique::Genetique(int Ni, Vecteur* distribi) : Cinnabar(Ni,"../logos/adn.txt",distribi)
{
		cout<<endl<<"Nombre d'individu de la population:"<<endl;
		cin>>pop;
		Tchemins=new int*[pop];
		for(int i=0;i<pop;i++)
			Tchemins[i]=new int[N+1];
		srand(time(NULL));
        int tmp;
        int* stock=new int[N]; //points (affiches) disponibles
        for(int i=1;i<N;i++)
                stock[i]=i;
        for(int i=0;i<pop;i++) //Pour chaque individu...
        {
                Tchemins[i][0]=0; //(On commence au point zero)
		Tchemins[i][N]=0;
                for(int j=1;j<N;j++) //...sur toutes les affiches...
                {
                        for(int k=0;k<N-j;k++)
                        {
                                tmp=rand() %(N-j);
                                tmp+=j;
                                Tchemins[i][j]=stock[tmp]; //...on remplit avec les points encore disponible...
                                permuter(stock,tmp,j); //.. et on enleve le point utiliser en le rangeant tel qu'il ne sorte plus a la prochaine itération.
                        }
                }
        }
        nbsurvivants=(pop/2);
		if(pop%2!=0)
			nbsurvivants+=1;
		Dchemins=new double[pop];
		fitness=new double[pop];
		survivants=new int[nbsurvivants];
		cout<<endl<<"Nombre d'iterations:"<<endl;
		cin>>nbiter;
		cout<<endl<<"Frequence de mutation:"<<endl;
		cin>>fmut;
		cout<<endl<<"Severite de la selection:"<<endl;
		cin>>alpha;
		cout<<endl<<"Population de depart:"<<endl;
		for(int i=0;i<pop;i++) //pour chaque individu
       		{
                	for(int j=0;j<N+1;j++) //sur toutes les affiches
                	{
				cout<<Tchemins[i][j];
			}
			cout<<endl<<endl;
		}
}


double Genetique::evalChemin()
{
	for(int i=0;i<pop;i++)
        {
                Dchemins[i]=0;
                for(int j=0;j<N;j++)
                {
                        Dchemins[i]+=Mdistances(Tchemins[i][j]+1,Tchemins[i][j+1]+1); //Distance du trajet qu'empreinte chaque individu
                }
        }
	return 0.;
}


void Genetique::generestant(int enfant, int parent,int geneconserve)
{
	bool existe;
	for(int i=1;i<N;i++)
	{
		if(i==geneconserve)
			i=(N-geneconserve);
		existe=false;
		for(int j=geneconserve;j<(N-geneconserve);j++)
		{
			if(Tchemins[parent][i]==Tchemins[enfant][j])
			{
				existe=true;
				break;
			}
		}
		if(existe==false)
		{
			for(int j=1;j<i;j++)
			{
				if(j==geneconserve)
					j=(N-geneconserve);
				if(Tchemins[enfant][j]==Tchemins[parent][i])
				{
					existe=true;
					break;
				}
			}
		}
		if(existe==false)
		{
			Tchemins[enfant][i]=Tchemins[parent][i];
		}
		if(existe==true)
		{
			for(int tmp=1;tmp<N;tmp++)
			{
				existe=false;
				for(int j=geneconserve;j<(N-geneconserve);j++)
				{
					if(Tchemins[enfant][j]==tmp)
					{
						existe=true;
						break;
					}
				}
				if(existe==false)
				{
					for(int j=1;j<i;j++)
					{
						if(j==geneconserve)
							j=(N-geneconserve);
						if(Tchemins[enfant][j]==tmp)
						{
							existe=true;
							break;
						}
					}
				}
				if(existe==false)
				{
					Tchemins[enfant][i]=tmp;
					break;
				}
			}
		}
	}
}


void Genetique::Fitness()
{
	double cheminTotal=0;
	double tmp=0;
	for(int i=0;i<pop;i++)
		cheminTotal+=Dchemins[i];
	for(int i=0;i<pop;i++)
	{
		fitness[i]=pow(cheminTotal/Dchemins[i],alpha); //Chaque indivu a un indice de performances
		tmp+=fitness[i];
	}
	for(int i=0;i<pop;i++)
		fitness[i]/=tmp; //on normalise pour avoir fitness entre 0 et 1 (pour SelectionRoulette)
}


void Genetique::SelectionRoulette()	//revoir cette partie
{
	srand(time(NULL));
	double rnd,tmp,moins;
	moins=0;
	for(int i=0;i<nbsurvivants;i++)
	{
		rnd=(1-moins)*((double)rand()/RAND_MAX); //On ajuste mle nombre aléatoire a  la somme des probabilités d'etre selectionner des individus restant
		tmp=0;
		for(int j=0;j<pop;j++)
		{
			tmp+=fitness[j];
			if(tmp>rnd) //Selection relative aux chances d'etre selectionner
			{
				survivants[i]=j; //On garde le numero des survivants
				moins+=fitness[j];
				fitness[j]=0; //Pour ne pas avoir deux fois le meme individu qui survit
				break;
			}
		}
	}
}


void Genetique::TriSurvivants()
{
	bool vivant[pop];
	for(int i=0;i<pop;i++)
	{
		vivant[i]=false;
		for(int j=0;j<nbsurvivants;j++)
		{
			if(i==survivants[j])
				vivant[i]=true; //On remplit un tableau de bool, correspondant au numero des individus ayant survecu
		}
	}
	for(int i=0;i<pop;i++)
	{
		if(vivant[i]==false)
		{
			for(int j=i;j<pop;j++)
			{
				if(vivant[j]==true)
				{
					for(int k=0;k<N;k++)
						Tchemins[i][k]=Tchemins[j][k]; //On pace les individus dans les nbsurvivants première case de la population
					vivant[j]=false;
					vivant[i]=true;
					break;
				}
			}
		}
	}
}


void Genetique::CroisementMPX()
{
		srand(time(NULL));
	int geneconserve=(N/2)-(N/4);
	int nbparent=nbsurvivants;
	int parent1,parent2;
	parent1=0;
	if(nbsurvivants%2!=0) //Si nbsurvivants impaire, un parent en trop, le "dernier" survivant n'aura pas d'enfant dans ce cas (aleatoire grace au rand de SelectionRoulette)
		nbparent--;
	//On boucle sur les case des individus mort -> qu'on rempliera comme enfant
	for(int i=nbsurvivants;i<pop-1;i+=2) //i+2 car deux parents donne deux enfant -> on remplit deux enfants par itérations
	{
		 //Chaque individu vivant sera au moins une fois parent (sauf cas impaire)
			do{
			parent2=rand() %nbsurvivants; //On tire le second parent de manière aléatoire
			}while(parent2==parent1);
			//On place ensuite les genes centraux
			for(int k=geneconserve;k<(N-geneconserve);k++)
			{
				Tchemins[i][k]=Tchemins[parent2][k]; //Enfant 1 prend les genes centraux de parent2
				Tchemins[i+1][k]=Tchemins[parent1][k];	//Enfant 2 prend les genes centraux de parent1
			}
			//Puis on complete les genes restants
			generestant(i,parent1,geneconserve); //pour l'enfant1
			generestant(i+1,parent2,geneconserve); //pour l'enfant2
			parent1++;
			if(parent1==nbparent)
				break;
	}
}


void Genetique::Mutation()
{
	srand(time(NULL));
	int mut,gmut1,gmut2;
	for(int i=0;i<pop;i++)
	{
		mut=rand() %fmut; //On tire au hasard pour chaque individu si il vont subir une mutation
		if(mut==1)
		{
			do{
			gmut1=(rand() %(N-1))+1; //On choisit deux genes a mut (permuter) au hasard sauf le premier
			gmut2=(rand() %(N-1))+1;
			}while(gmut1==gmut2);
			permuter(Tchemins[i],gmut1,gmut2); //On effectue la permutation des deux gène a muter
		}
	}
}


void Genetique::CheminFinal()
{
	Fitness();
	int max=0;
	double disttot=0;
	for(int i=0;i<pop;i++)
	{
		if(fitness[i]>fitness[max])
			max=i;
	}
	cout<<endl<<"Chemin final le plus court:"<<endl;
	for(int i=0;i<N+1;i++)
		cout<<Tchemins[max][i]<<"=>";
	cout<<"fin."<<endl;
	cout<<"Distance total:";
	for(int i=0;i<N;i++)
		disttot+=Mdistances(Tchemins[max][i]+1,Tchemins[max][i+1]+1);
	cout<<disttot<<endl<<endl;
}


void Genetique::Resolution()
{
	char graph;	//Y ou N, graph ou pas graph
	int f;	//frequence d'enregistrement des graphs (pour faire de belles vidéos
	string snomdossier;
	string snomdossier2;
	cout<<"enregistrer un graphique ?"<<endl<<"(Y/N)"<<endl;
	do{
	cin>>graph;}while(graph!='Y' and graph!='N');
	if(graph=='N')
		f=nbiter+42;
	if(graph=='Y')
	{
	cout<<"nom du dossier =?"<<endl;
	cin>>snomdossier;
	snomdossier="../Resultats/"+snomdossier;
	mkdirs(snomdossier);	//fabrique notre arborescence 
	cout<<endl<<"frequence =?"<<endl;
	do{
	cin>>f;}while(f<0 and f>nbiter);
	}

	for(int k=1;k<=nbiter;k++)
	{
		evalChemin();
		Fitness();
		SelectionRoulette();
		TriSurvivants();
		CroisementMPX();
		Mutation();
		if(k%f==0)
			groot(k,snomdossier);
	}
	if(graph!='N')
	{
		avi(nbiter,f,snomdossier);
	}

	CheminFinal();
}


Genetique::~Genetique()
{
	for(int i=0;i<pop;i++)
		delete[] Tchemins[i];
	delete[] Tchemins;
	delete[] survivants;
	delete[] Dchemins;
	delete[] fitness;	
}

void Genetique::groot(int k, string snomdossier)
{
	string sroot=snomdossier+"/graph/graph";        //le chemin où sera ranger nos graphs
        ostringstream sstemp;
        sstemp<<k;      //pour numéroter nos graph
        sroot+=sstemp.str();
        sroot+=".root"; //au final le nom donne ../Resultats/nomdudossier/graph/graph0001.root
        TCanvas *c = new TCanvas;       //on creer un TCanvas
        const char* croot=sroot.c_str();        //      le nom doit être un char*
        TFile fichier(croot,"RECREATE");        //on creer notre fichier rot
        double villesX[N+1],villesY[N+1];   //les coordonnées de nos villes
        Vecteur Vtemp(2);       //vecteur temporaire pour les coord des nos villes
	int max=0;
        for(int i=1;i<pop;i++)
        {
                if(fitness[i]>fitness[max])
                        max=i;
        }
        for(int i=0;i<N+1;i++)    //on remple les tableaux
        {
                Vtemp=*(distrib+Tchemins[max][i]); //On trace les villes dans l'ordre du meilleur chemin (meilleur individu avec la meileure fit)
                villesX[i]=Vtemp(1);
                villesY[i]=Vtemp(2);
        }
        TGraph *villes=new TGraph(N+1,villesX,villesY);   //on creer le graph de nos villes
        villes->SetMarkerStyle(29);     //des belles étoiles
        villes->SetMarkerColor(2);      //rouge
        villes->SetMarkerSize(3);       //assez gros
        villes->SetLineStyle(1);      
	villes->SetLineWidth(2);
	villes->SetTitle("Genetique");
        villes->Write();        //on écrit
        villes->Draw();
		sroot=snomdossier+"/png/graph"; //le chemin pour l'export en png
        if(k<10)        //on normalise les noms (ordre alphabétique), cela fonctionnera tant qu'il y a $
                sroot+="000";
        if(k<100 and k>9)
                sroot+="00";
        if(k<1000 and k>99)
                sroot+="0";
        sroot+=sstemp.str();
        sroot+=".png";          //au final le nom donne ../Resultats/png/nomdudossier/graph0001.png etc$
        croot=sroot.c_str();
        c->Print(croot);        //on exporte en png
        fichier.Close();        //on ferme le fichier root
}


/******************************fonction de la classe tabu*******************************************/


RechercheTabou::RechercheTabou(int Ni, Vecteur* distribi) : Cinnabar(Ni,"../logos/tabu.txt",distribi)
{
	cout<<endl<<"Nombre d'iterations:"<<endl;
	cin>>nbiter;
	cout<<endl<<"Taille du tableau (memoire Tabu):"<<endl;
	cin>>mem;
	cout<<endl<<"Nombre de Candidats:"<<endl;
	cin>>nCandidats;
	Tchemins=new int*[nCandidats];
	for(int i=0;i<nCandidats;i++)
		Tchemins[i]=new int[N+1]; //Car on boucle sur le premier point
	mSolution=new int[N+1]; //Meilleur solution general
	mCandidat=new int[N+1]; //Meilleur solution local=Meilleur Candidat
	TabuList=new int*[mem];
	for(int i=0;i<mem;i++)
		TabuList[i]=new int[N+1]; 
	fitness=new double[nCandidats];
	mSolution[0]=0;
	mSolution[N]=0;
	mCandidat[0]=0;
	mCandidat[N]=0;
	int tmp;
	int* stock=new int[N];
	for(int i=1;i<N;i++)
		stock[i]=i;
	for(int i=1;i<N;i++)
	{
		tmp=rand() %(N-i);
		tmp+=i;
		mSolution[i]=stock[tmp]; //On initialise une solution aleatoire
		permuter(stock,tmp,i);
		mCandidat[i]=mSolution[i]; //On initialise la meilleur solution local a la solution global aleatoire
	}
	for(int i=0;i<mem;i++)
	{
		for(int j=0;j<N+1;j++)
			TabuList[i][j]=0; //On initialise notre liste Tabu a 0 partout
	}
	for(int i=0;i<nCandidats;i++)
	{
		Tchemins[i][0]=0;
		Tchemins[i][N]=0;
	}
	delete stock;
}


double RechercheTabou::evalChemin()
{
        for(int i=0;i<nCandidats;i++)
        {
                fitness[i]=0;
                for(int j=0;j<N;j++)
                {
                        fitness[i]+=Mdistances(Tchemins[i][j]+1,Tchemins[i][j+1]+1); //Distance pour chaque Candidat (correspond a leur performance)
                }
        }
	return 0;
}

void RechercheTabou::Candidat()
{
	int tmp=1;
	int perm=1;
	for(int i=0;i<nCandidats;i++)
        {
                for(int j=1;j<N;j++)
                        Tchemins[i][j]=mCandidat[j]; //Nos nouveaux candidats sont d'abord initialisé a la meilleur solution local trouvé
		if(tmp==N) //Une fois toutes les villes permuter avec la nème ville, on continue en permutant toute les villes avec la n+1éme ville
		{
		//	tmp=1;			méthode sans rand
		//	perm++;			méthode sans rand
		}
		if(perm>=N)
	//		perm=1;		méthode sans rand
		tmp=rand()%(N-1)+1;
		perm=rand()%(N-1)+1;
		permuter(Tchemins[i],tmp,perm); //On permute deux villes pour obtenir notre nouvelle population
	//	tmp++;			méthode sans rand
        }
}

bool RechercheTabou::Tabu(int k)
{
	bool tabu;
	for(int i=0;i<mem;i++)
	{
		tabu=true;
		for(int j=1;j<N;j++)
		{
			if(Tchemins[k][j]!=TabuList[i][j])
			{
				tabu=false;
				break;
			}
		}
		if(tabu)
		{
			return tabu;
		}
	}
	return tabu;
}


void RechercheTabou::afficherres()
{
	cout<<endl<<"Meilleur Solution : ";
	for(int i=0;i<N+1;i++)
		cout<<mSolution[i]<<"=>";
	cout<<"fin."<<endl;
	cout<<"Distance :"<<fitmSolution<<endl;

}
void RechercheTabou::TabuSearch()
{
	char graph;	//Y ou N, graph ou pas graph
	int f;	//frequence d'enregistrement des graphs (pour faire de belles vidéos
	string snomdossier;
	string snomdossier2;
	cout<<"enregistrer un graphique ?"<<endl<<"(Y/N)"<<endl;
	do{
	cin>>graph;}while(graph!='Y' and graph!='N');
	if(graph=='N')
		f=nbiter+42;
	if(graph=='Y')
	{
	cout<<"nom du dossier =?"<<endl;
	cin>>snomdossier;
	snomdossier="../Resultats/"+snomdossier;
	mkdirs(snomdossier);	//fabrique notre arborescence 
	cout<<endl<<"frequence =?"<<endl;
	cin>>f;
	}
	fitmSolution=0; //performance de la meilleure solution
	double fitmCandidat=DBL_MAX; //performance du meilleur candidats
	int actualmem=-1; //coresond au nombre de case tabu remli, revient a 0 une fois la TabuListe remplie, incrémente a chque itération  (FIFO)
	bool tabu;
	for(int i=0;i<N;i++)
		fitmSolution+=Mdistances(mSolution[i]+1,mSolution[i+1]+1); //Distance/performance de la solution intial
	for(int i=1;i<=nbiter;i++)
	{
			Candidat(); //on creer nos candidats au voisinage de Msolution
			evalChemin(); //On calcul leur performance c est a dire leur distance
			fitmCandidat=DBL_MAX;
			for(int j=0;j<nCandidats;j++)
			{
				tabu=Tabu(j);
				if(tabu==false && fitness[j]<fitmCandidat)
				{
					for(int k=1;k<N;k++)
					mCandidat[k]=Tchemins[j][k];
					fitmCandidat=fitness[j];
				}
			}
			if(fitmCandidat<fitmSolution)
			{
				fitmSolution=fitmCandidat;
				for(int k=1;k<N;k++)
					mSolution[k]=mCandidat[k];
			}
			if(actualmem==mem-1)
				actualmem=0;
			else
				actualmem++;
			for(int j=1;j<N;j++)
			{
				TabuList[actualmem][j]=mCandidat[j];
			}
			
			if(i%f==0)
				groot(i,snomdossier);
	

	}
	afficherres();
	system("read typhlosion");
	if(graph!='N')
	{
		system("clear");
		cout<<"Enregistrer un film avi ?"<<endl<<"(Y/N)"<<endl;
		do{
		cin>>graph;}while(graph!='Y' and graph!='N');
		if(graph=='Y')
		{
			avi(nbiter,f,snomdossier);		
		}
	}

}

void RechercheTabou::groot(int k,string snomdossier)
{
	string sroot=snomdossier+"/graph/graph";        //le chemin où sera ranger nos graphs
        ostringstream sstemp;
        sstemp<<k;      //pour numéroter nos graph
        sroot+=sstemp.str();
        sroot+=".root"; //au final le nom donne ../Resultats/nomdudossier/graph/graph0001.root
        TCanvas *c = new TCanvas;       //on creer un TCanvas
        const char* croot=sroot.c_str();        //      le nom doit être un char*
        TFile fichier(croot,"RECREATE");        //on creer notre fichier rot
	double villesX[N+1],villesY[N+1];   //les coordonnées de nos villes
        Vecteur Vtemp(2);       //vecteur temporaire pour les coord des nos villes
        for(int i=0;i<N+1;i++)    //on remple les tableaux
        {
		Vtemp=*(distrib+mSolution[i]);
		villesX[i]=Vtemp(1);
		villesY[i]=Vtemp(2);
	}
	TGraph *villes=new TGraph(N+1,villesX,villesY);   //on creer le graph de nos villes
        villes->SetMarkerStyle(29);     //des belles étoiles
        villes->SetMarkerColor(2);      //rouge
	villes->SetMarkerSize(3);       //assez gros
	villes->SetLineStyle(1);	//De superbe lignes pleines
        villes->SetLineColor(1);        //noires,
	villes->SetLineWidth(2);	//et suffisament épaissent, reliant les villes selons l'orde de la meilleure solution
	villes->SetTitle("tabu");
        villes->Write();        //on écrit
	villes->Draw();
	double villesXC[N+1],villesYC[N+1];
	for(int i=0;i<N+1;i++)
	{
		Vtemp=*(distrib+mCandidat[i]);
		villesXC[i]=Vtemp(1);
                villesYC[i]=Vtemp(2);
	}
	TGraph *candidat=new TGraph(N+1,villesXC,villesYC);	
        candidat->SetMarkerStyle(0);       //On ne met pas les points des villes puisque tracés précédemment
        candidat->SetLineStyle(1);        //De superbe lignes pleines
        candidat->SetLineColor(4);        //bleues,
        candidat->SetLineWidth(1);        //et assez fines, reliant les villes selons l'orde de la meilleure solution
        candidat->Write();        //on écrit
        candidat->Draw("L");	//on superpose par dessus le graph ville précédent
	sroot=snomdossier+"/png/graph"; //le chemin pour l'export en png
        if(k<10)        //on normalise les noms (ordre alphabétique), cela fonctionnera tant qu'il y a moins de 9999 itérations (au pire rajouter des zéros si il en faut plus)
                sroot+="000";
        if(k<100 and k>9)
                sroot+="00";
        if(k<1000 and k>99)
                sroot+="0";
        sroot+=sstemp.str();
        sroot+=".png";          //au final le nom donne ../Resultats/png/nomdudossier/graph0001.png etc...
        croot=sroot.c_str();;
        c->Print(croot);        //on exporte en png
        fichier.Close();        //on ferme le fichier root
}


RechercheTabou::~RechercheTabou()
{
delete[] mSolution;
delete[] mCandidat;
for(int i=0;i<mem;i++)
	delete[] TabuList[i];
delete[] fitness;
for(int i=0;i<nCandidats;i++)
	delete[] Tchemins[i];
}
