#include <fstream>
#include <iostream>
#include <iomanip>

#include <SysLin.h>
#include <VecMat.h>
#include "TFile.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "FitLineaire.h"

using namespace std;

// *************************************************************

/* M�thode ma�tresse qui r�alise l'ajustement sur un fichier 
   de donn�es (pas demande dans le TP, mais bien pratique) */
void FitLineaire::ajuster_donnees(const char * nom_fichier)
{
  lire_donnees(nom_fichier);
  construire_eqnormales();
  resoudre_eqnormales();
}

// *************************************************************

/* Lecture et stockage de donn�es � partir d'un fichier */
void FitLineaire::lire_donnees(const char * nom_fichier)
{
  int dim = 10, i = 1;
  ifstream fichier_donnees(nom_fichier);
  assert(fichier_donnees);
  N = 0 ; 
  x.redim(dim); y.redim(dim); sig.redim(dim);
  do {
    fichier_donnees >> x(i) >> y(i) >> sig(i);
    if ( !fichier_donnees ) break;
    i++; 
    if ( i > dim ) {
      dim *= 2;
      x.redim(dim); y.redim(dim); sig.redim(dim);
    }
  } while( !fichier_donnees.eof() );
  fichier_donnees.close();
  N = i-1;
  x.redim(N); y.redim(N); sig.redim(N);
}


// *************************************************************
//                         CALCUL
// *************************************************************

// eval_f d�finit les fonctions de base : virtual Vecteur eval_f(double x)=0;
Vecteur FitHarmonique::eval_f(double x)	//modele harmonique
{
Vecteur tst(M);
for(int i=1;i<M+1;i++)
{
tst(i)=cos(i*omega*x);
}
return tst;
}

Vecteur FitPolynom::eval_f(double x) //modele polynomiale
{
Vecteur eval(M);
double tmp;
tmp=1;
eval(1)=1;
for(int i=2;i<M+1;i++)
{
tmp*=x;
eval(i)=tmp;
}
return eval;
}

/* Construction de la matrice alpha et du vecteur beta
   des �quations normales */
void FitLineaire::construire_eqnormales()
{
	alpha.redim(M);
	beta.redim(M);
	double yi, fik,si,xi,si2;
	double btemp;//accumulateur pour beta
	double gtemp;//variable temporaire pour gamma et beta (optimisation)
	double atemp;//accumulateur pour alp�a
        gamma=0;
	for(int k=1;k<=M;k++)
	{
		btemp=0;
		for(int j=1;j<=M;j++)
		{
			atemp=0;
			for(int i=1;i<=N;i++)
						{
							yi=y(i); //variables auxiliaire, optimisation
							xi=x(i);//
							fik=eval_f(xi)(k);//
							si=sig(i);//
							si2=si*si;//
							atemp+=eval_f(xi)(j)*fik/(si2);//
							if(j==1)
							{
								gtemp=yi/(si2);//gtemp sert pour gamma et beta
								if(k==1){gamma+=yi*gtemp;}	//uniquement besoin une fois
								btemp+=fik*gtemp;	
							}

						} 
			alpha(k,j)=atemp;
		}
		beta(k)=btemp;
	}
}

/* R�solution des �quations normales */
void FitLineaire::resoudre_eqnormales()
{
	SystemeLineaire Equa(alpha); //on cree un objet sys lineaire avec notre matrice alpha
	Equa.Set_b(beta); //on configure b comme etant beta
	Equa.resoudreLU(); //on trouve la solution
	Equa.solutionLU();//
	a_sol=Equa.GetSolution();//et on remplit le vecteur a_sol
}
// Incertitudes : matrice de covariance
MatriceCarree FitLineaire::matrice_covariance()
{
  return alpha.inverse();
}


// *************************************************************
//                         RESULTATS
// *************************************************************

// Ecriture des parametres et incertitudes, 
// en fonction de l'indice de la fonction de base
void FitLineaire::ecrire_params(const char * nom_fichier)
{
  TFile fichier(nom_fichier,"RECREATE");
  TGraphErrors graph_error(M);
  graph_error.SetName("params");
  graph_error.SetTitle("Param�tres ajust�s");
  MatriceCarree C = matrice_covariance();

  for (int j = 1; j <= M; j++) {
    graph_error.SetPoint(j,j,a_sol(j));
    graph_error.SetPointError(j,0,sqrt(C(j, j)));
  }
  
  graph_error.Write();
  fichier.Close();
}

/* Evaluation de la fonction d'ajustement en un point */
double FitLineaire::eval_solution(double x)
{
  double mewtwo; //marre de toto!!!!
  mewtwo=eval_f(x)*a_sol;
  return mewtwo;
}

// chi2
double FitLineaire::chi2()
{
  double chi ;  
  chi=a_sol*(alpha*a_sol)-(2*a_sol*beta)+gamma;	//formule du cours
  chi/=N;
  return chi ;
}

/* Ecriture des valeurs de la fonction d'ajustement et des donn�es
   dans un fichier */
void FitLineaire::ecrire_solution(const char * nom_fichier)
{
  TFile fichier(nom_fichier,"RECREATE");
  TGraphErrors graph_data(N);
  graph_data.SetName("data");
  graph_data.SetTitle("Donn�es");
  TGraph graph_calcul(N);
  graph_calcul.SetName("y_de_x");

  for (int i = 1; i <= N; i++) {
    graph_calcul.SetPoint(i,x(i),eval_solution(x(i)));
    graph_data.SetPoint(i,x(i),y(i));
    graph_data.SetPointError(i,0.,sig(i));
  }
  graph_calcul.Write();
  graph_data.Write();
  fichier.Close();

}

